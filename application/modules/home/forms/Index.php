<?php

class Home_Form_Index extends Zend_Form {

   public function init() {
        
        $this->setName('index');
        $this->setAttrib( 'class', 'zend_form' );
        $this->setMethod('POST');
        
        $input = new Zend_Form_Element_Text('input');
        $input->setLabel('input:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->setAttrib('class', 'form-control')
                ->setAttrib('title', 'Informe seu nome')
                ->setDecorators(array(
                    'Label',
                    'ViewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag',array('tag'=>'div','class'=>'col-lg-3 marginBot'))
                    ));

        $result = new Zend_Form_Element_Text('result');
        $result->setLabel('result:')
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->setAttrib('class', 'form-control')
                ->setAttrib('title', 'Informe seu nome')
                ->setDecorators(array(
                    'Label',
                    'ViewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag',array('tag'=>'div','class'=>'col-lg-3 marginBot'))
                    ));

        $submit = new Zend_Form_Element_Submit('Salvar');
        $submit->setAttrib('class', 'btn btn-large btn-success col-lg-1 col-md-2 col-sm-2 col-xs-3 btSub')
                    ->setIgnore(true)
                    ->setDecorators(array(
                    
                    'ViewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag',array('tag'=>'div','class'=>'col-lg-12 col-lg-12 col-lg-12 col-lg-12'))
                    ));
        
        $this->addElements(array($input, $result, $submit));
    }
}