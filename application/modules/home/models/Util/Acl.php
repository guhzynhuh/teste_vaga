<?php


class Home_Model_Util_Acl
{
   

    private $autorizado;

    public function __construct($user,$param)
    {
    	$this->setAutorizado($user,$param);
		
    }

    public function setAutorizado($user,$param)
    {
        $acl = new Zend_Acl();
    	$acl->addRole(new Zend_Acl_Role('VISITANTE'))
    		//define o USUARIO herdando todos os acessos de VISITANTE
    		->addRole(new Zend_Acl_Role('USUARIO'),'VISITANTE');

		//Modules
		$acl->add(new Zend_Acl_Resource('home'));
		//Controller
		$acl->add(new Zend_Acl_Resource('index'));
		$acl->add(new Zend_Acl_Resource('login'));
		$acl->add(new Zend_Acl_Resource('cadastro'));
		$acl->add(new Zend_Acl_Resource('usuario'));
		
		$acl->add(new Zend_Acl_Resource('compra'));

		//Regras Menu:
		$acl->add(new Zend_Acl_Resource('menuLogin'));

		//Visitante pode acessar o Module site
		$acl->allow('VISITANTE',array('home'));
		//Acessos aoa Controllers do site
		$acl->allow('VISITANTE',array('index'));
		$acl->allow('VISITANTE',array('cadastro'));

		$acl->allow('VISITANTE',array('menuLogin'));
		
		$acl->allow('USUARIO',array('compra'));
		$acl->allow('USUARIO',array('usuario'));
		//usuario não pode acessar o cadastro do site
		$acl->deny('USUARIO',array('cadastro'));
		$acl->deny('USUARIO',array('menuLogin'));

		$test = 0;
		foreach ($param as $key => $value) {
			if($acl->isAllowed($user,$value)){
			
			}else{
				$test++;
			}
		}
		if(!$test){
			$this->autorizado = true;
		}else{
			$this->autorizado = false;
		}

		return $this->autorizado;
    }

    /**
     * Gets the value of autorizado.
     *
     * @return mixed
     */
    public function getAutorizado()
    {
        return $this->autorizado;
    }

    /*if($acl->isAllowed('admin',$request->getModuleName())){
			var_dump("Acesso permitido");
		}else{
			var_dump("Acesso negado");
		}
	Zend_Controller_Request_Abstract

		*/



    
}