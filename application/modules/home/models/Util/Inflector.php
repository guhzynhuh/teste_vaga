<?php

define("STR_REDUCE_LEFT", 1);
define("STR_REDUCE_RIGHT", 2);
define("STR_REDUCE_CENTER", 4);


class Home_Model_Util_Inflector
{
   

    public function accent_remove($str) {
        $from = array(
            '@' => 'a', '&' => 'e', 'Œ' => 'd', 'œ' => 'd',
            'À' => 'a', 'Á' => 'a', 'Â' => 'a', 'Ã' => 'a', 'Ä' => 'a', 'Å' => 'a', 'Æ' => 'a',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a',
            'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Õ' => 'o', 'Ö' => 'o', 'Ø' => 'o',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
            'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
            '$' => 's', '§' => 's', 'Š' => 's', 'š' => 's',
            'Ý' => 'y', 'Ÿ' => 'y', 'ý' => 'y', 'ÿ' => 'y', '¥' => 'y',
            'Ž' => 'z', 'Ç' => 'c', 'Ð' => 'd', 'Ñ' => 'n', 'ß' => 'b', 'µ' => 'u',
            'ž' => 'z', 'ç' => 'c', 'ð' => 'd', 'ñ' => 'n',
        );

        return str_replace(array_keys($from), array_values($from), $str);
        
    }

    
    public function resumeStr($texto,$tamanho,$find = null){
        
        if($find == null){
            $find = "[...]";
        }
        $texto = explode(" ", substr($texto, 0, $tamanho));
        if(count($texto) > 1) array_pop($texto);
        $texto = implode(" ", $texto).' '.$find;
        return $texto;
    }

    public function trata_url($str, $replace = '_') {
        $from = array(
            '@' => 'a', '&' => 'e', 'Œ' => 'd', 'œ' => 'd',
            'À' => 'a', 'Á' => 'a', 'Â' => 'a', 'Ã' => 'a', 'Ä' => 'a', 'Å' => 'a', 'Æ' => 'a',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a',
            'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e',
            'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
            'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Õ' => 'o', 'Ö' => 'o', 'Ø' => 'o',
            'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o',
            'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u',
            'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u',
            '$' => 's', '§' => 's', 'Š' => 's', 'š' => 's',
            'Ý' => 'y', 'Ÿ' => 'y', 'ý' => 'y', 'ÿ' => 'y', '¥' => 'y',
            'Ž' => 'z', 'Ç' => 'c', 'Ð' => 'd', 'Ñ' => 'n', 'ß' => 'b', 'µ' => 'u',
            'ž' => 'z', 'ç' => 'c', 'ð' => 'd', 'ñ' => 'n',
        );

        $str = str_replace(array_keys($from), array_values($from), $str);
        return strtolower(trim(preg_replace('/\_+/is', $replace, preg_replace("#[^A-Z0-9]#is", '_', $str)), '_'));
    }

    

    
   



    
}