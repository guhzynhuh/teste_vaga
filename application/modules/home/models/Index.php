<?php

class Home_Model_Index extends Zend_Db_Table
{
	protected $_name = 'teste';

	public function listar(){
		try{
			$sql = $this->select();
			$sql->from($this->_name);

			$return = $this->fetchAll($sql);
			if($return){
				return $return->toArray();
			}else{
				return false;
			}
		}catch(Zend_Db_Exception $e){}
	}
	
	public function inserir(Array $dados){
		try{
			$dados = self::colunas($dados);

			return parent::insert($dados);
			
		}catch(Zend_Db_Exception $e){
			

		}
	}
	public function update(Array $dados, $id){
		try{
			$where = $this->getAdapter()->quoteInto('ID = ?',$id);
			
			$dados = self::colunas($dados);
			return parent::update($dados,$where);
			
		}catch(Zend_Db_Exception $e){

		}
	}

	protected function colunas(Array $dados){
		$ret =  array();
		foreach ($dados as $coluna => $valor) {
			if(in_array($coluna, $this->_getCols()))
				$ret[$coluna] = $valor;
		}
		return $ret;
	}
}