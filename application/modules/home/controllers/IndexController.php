<?php

class Home_IndexController extends Zend_Controller_Action {

    public function indexAction() {
        $this->_helper->layout->setLayout('layout');

        $mod = new Home_Model_Index();
        $form = new Home_Form_Index();

        if($this->_request->isPost()){
            if ($this->_request->getpost()) {    
    			$data = array();

                $data['input'] = $_POST['input'];
                $data['result'] = $_POST['result'];

    			$mod->inserir($data);
            }
		}

		$listar = $mod->listar();
		$this->view->listar = $listar;

        $this->view->form = $form;
    }
}